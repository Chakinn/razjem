
USE razjemDB;
DROP TABLE IF EXISTS menu_order_item_id;
DROP TABLE IF EXISTS order_order_item_id;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS addresses;
DROP TABLE IF EXISTS restaurants;
DROP TABLE IF EXISTS menus;
DROP TABLE IF EXISTS menu_items;
DROP TABLE IF EXISTS menus_menu_items;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS order_items;
DROP TABLE IF EXISTS orders_order_items;
DROP TABLE IF EXISTS lobbys;
DROP TABLE IF EXISTS lobbys_users;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255),
    email VARCHAR(255),
    password VARCHAR(255),
    wallet INT,
    PRIMARY KEY (id)
);

CREATE TABLE addresses (
    id INT NOT NULL AUTO_INCREMENT,
    city VARCHAR(255),
    street VARCHAR(255),
    house_number INT,
    postal_code VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE menus (
    id INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY(id)
);

CREATE TABLE menu_items (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    price INT,
    description VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE menus_menu_items (
    menu_id INT,
    menu_item_id INT,
    FOREIGN KEY (menu_id) REFERENCES menus(id),
    FOREIGN KEY (menu_item_id) REFERENCES menu_items(id)  
);

CREATE TABLE restaurants (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255),
    address_id INT,
    menu_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (address_id) REFERENCES addresses(id),
    FOREIGN KEY (menu_id) REFERENCES menus(id)

);

CREATE TABLE orders (
    id INT NOT NULL AUTO_INCREMENT,
    address_id INT,
    restaurant_id INT,
    order_time VARCHAR(255),
    order_status INT,
    PRIMARY KEY (id),
    FOREIGN KEY (address_id) REFERENCES addresses(id),
    FOREIGN KEY (restaurant_id) REFERENCES restaurants(id)
);

CREATE TABLE order_items(
    id INT NOT NULL AUTO_INCREMENT,
    menu_item_id INT,
    user_id INT,
    quantity INT,
    PRIMARY KEY (id),
    FOREIGN KEY (menu_item_id) REFERENCES menu_items(id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE orders_order_items(
    order_id INT,
    order_item_id INT,
    FOREIGN KEY (order_id) REFERENCES orders(id),
    FOREIGN KEY (order_item_id) REFERENCES order_items(id)  
);

CREATE TABLE lobbys(
	id INT NOT NULL AUTO_INCREMENT,
    order_id INT,
    owner_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (order_id) REFERENCES orders(id),
    FOREIGN KEY (owner_id) REFERENCES users(id)
);

CREATE TABLE lobbys_users(
    lobby_id INT,
    user_id INT,
    FOREIGN KEY (lobby_id) REFERENCES lobbys(id),
    FOREIGN KEY (user_id) REFERENCES users(id)
);
























