var userInput = document.getElementById("userInput");
var __username = userInput.value;
var __lobbyId = 0;
const server_url='http://45.151.125.16:8080/chakinos/razjemAPI/1.0.0';
const left = document.getElementById("left");
const right = document.getElementById("right");

document.getElementById("addressButton").onclick = () => {selectAddress()}
document.getElementById("userButton").onclick = () => {__username = userInput.value}
function register(username, password, email) {
    
}

function login(username, password) {

}

function fetchLobbys() {
    const url = server_url+"/lobby";
    fetch(url)
    .then(data=>{return data.json()})
    .then(lobbys=>showLobbys(lobbys))
    .catch(error=>(console.log(error)));
}

function showLobbys(lobbys) {
    for(let i = 0; i < lobbys.length; i++) {
        const lobbyElement = document.createElement("div");
        lobbyElement.classList.add("lobby");
        lobbyElement.onclick = () => {joinLobby(lobbys[i].id, __username)}

    
        const nameElement = document.createElement("p");
        const nameElementText = document.createTextNode(lobbys[i].order.restaurant.name)
        nameElement.appendChild(nameElementText);

        const ownerElement = document.createElement("p");
        const ownerElementText = document.createTextNode(lobbys[i].ownerUsername);
        ownerElement.appendChild(ownerElementText);

        lobbyElement.appendChild(nameElement);
        lobbyElement.appendChild(ownerElement);

        left.appendChild(lobbyElement);

    }
    button = document.createElement("button");
    buttonText = document.createTextNode("utwórz");
    button.appendChild(buttonText);
    button.onclick = () => {createLobby(__username)};
    button.classList.add("but");    

    left.appendChild(button);
}

function createLobby(ownerUsername) {
    const url = new URL(server_url+"/lobby/new")
    const params = {ownerUsername: ownerUsername};
    addParams(url,params);
    fetch(url)
    .then(data=>{return data.json()})
    .then(lobby=>showLobby(lobby))
    .catch(error=>(console.log(error)));
}

function joinLobby(lobbyId, clientUsername) {
    const url = new URL(server_url+"/lobby/join")
    const params = {lobbyId: lobbyId, clientUsername: clientUsername};
    addParams(url, params)
    fetch(url)
    .then(data=>{return data.json()})
    .then(lobby=>showLobby(lobby))
    .catch(error=>(console.log(error)));
}

function selectLobby(lobbyId) {
    const url = server_url+"/lobby/"+lobbyId;
    fetch(url)
    .then(data=>{return data.json()})
    .then(lobby=>showLobby(lobby))
    .catch(error=>(console.log(error)));
}

function showLobby(lobby) {
    __lobbyId = lobby.id;
    clearContent();
    const clients = document.createElement("div");
    clients.classList.add("clients");

    const clientheader = document.createElement("h4");
    const clientheaderText = document.createTextNode("Clients:");
    clientheader.appendChild(clientheaderText);
    clients.appendChild(clientheader);

    for(let i = 0; i < lobby.clients.length; i++) {
        const clientElement = document.createElement("p");
        const clientElementText = document.createTextNode(lobby.clients[i]);
        clientElement.appendChild(clientElementText);
        clients.appendChild(clientElement);
    }

    const order = document.createElement("div");
    order.classList.add("order")

    const address = document.createElement("div");
    address.classList.add("address");

    const cityElement = document.createElement("p");
    const cityElementText = document.createTextNode(lobby.order.address.city);
    cityElement.appendChild(cityElementText);
    address.appendChild(cityElement);
    const streetElement = document.createElement("p");
    const streetElementText = document.createTextNode(lobby.order.address.street+" "+lobby.order.address.houseNumber);
    streetElement.appendChild(streetElementText);
    address.appendChild(streetElement);
    const postalCodeElement = document.createElement("p");
    const postalCodeElementText = document.createTextNode(lobby.order.address.postalCode);
    postalCodeElement.appendChild(postalCodeElementText);
    address.appendChild(postalCodeElement);


    const orderItems = document.createElement("div");
    const orderItemsHeader = document.createElement("h4");
    const orderItemsHeaderText = document.createTextNode("Your items:");
    orderItemsHeader.appendChild(orderItemsHeaderText);
    orderItems.appendChild(orderItemsHeader);
    for(let i = 0; i < lobby.order.orderItems.length; i++) {
        const orderItem = lobby.order.orderItems[i];

        if(orderItem.clientUsername !== __username) {
            continue;
        }

        const orderItemElement = document.createElement("div");
        orderItemElement.classList.add("orderItem");
        
        const orderItemNameElement = document.createElement("p");
        const orderItemNameElementText = document.createTextNode(orderItem.menuItem.name);
        orderItemNameElement.appendChild(orderItemNameElementText);
        orderItemElement.appendChild(orderItemNameElement);

        const orderItemQuantityElement = document.createElement("p");
        const orderItemQuantityElementText = document.createTextNode(orderItem.quantity);
        orderItemQuantityElement.appendChild(orderItemQuantityElementText);
        orderItemElement.appendChild(orderItemQuantityElement);
        
        orderItems.appendChild(orderItemElement);
    }

    const restaurantName = document.createElement("p");
    const restaurantNameText = document.createTextNode(lobby.order.restaurant.name);
    restaurantName.appendChild(restaurantNameText);

    const menu = document.createElement("div");
    menu.classList.add("menu");
    for(let i = 0; i < lobby.order.restaurant.menu.menuItems.length; i++) {
        const menuItem = lobby.order.restaurant.menu.menuItems[i];

        const menuItemElement = document.createElement("div");
        menuItemElement.classList.add("menuItem");

        const menuItemNameElement = document.createElement("p");
        menuItemNameElement.onclick = () => {addOrderItem(menuItem)};
        const menuItemNameElementText = document.createTextNode(menuItem.name);
        menuItemNameElement.appendChild(menuItemNameElementText);
        menuItemElement.appendChild(menuItemNameElement);     
        
        const menuItemPriceElement = document.createElement("p");
        const menuItemPriceElementText = document.createTextNode(menuItem.price);
        menuItemPriceElement.appendChild(menuItemPriceElementText);
        menuItemElement.appendChild(menuItemPriceElement);     

        const menuItemDescElement = document.createElement("p");
        const menuItemDescElementText = document.createTextNode(menuItem.description);
        menuItemDescElement.appendChild(menuItemDescElementText);
        menuItemElement.appendChild(menuItemDescElement);   
        
        menu.appendChild(menuItemElement);

    }

    restaurant = document.createElement("div")
    restaurant.appendChild(restaurantName);
    restaurant.appendChild(menu);

    left.appendChild(address);
    left.appendChild(clients);
    left.appendChild(orderItems);
    left.appendChild(restaurant);

    fetchRestaurants()
}



function fetchRestaurants() {
    const url = server_url+"/restaurant";
    fetch(url)
    .then(data=>{return data.json()})
    .then(restaurants=>showRestaurants(restaurants))
    .catch(error=>(console.log(error)));
}

function showRestaurants(restaurants) {
    restaurantNames = document.createElement("div");
    for(let i = 0; i < restaurants.length; i++) {
        restaurantElement = document.createElement("p");
        restaurantElement.classList.add("restaurant")
        restaurantElementText = document.createTextNode(restaurants[i].name);
        restaurantElement.appendChild(restaurantElementText);
        restaurantElement.onclick = () => selectRestaurant(restaurants[i].id);
        restaurantNames.appendChild(restaurantElement);
    }

    right.appendChild(restaurantNames)

}

function selectRestaurant(restaurantId) {
    const restaurantIdObject = {restaurantId};

    const url = new URL(server_url+"/order/restaurant")
    const params = {lobbyId: __lobbyId, ownerUsername: __username};
    addParams(url, params)
    const additionalParams = {body: JSON.stringify(restaurantId), method:"POST", headers: {"content-type": "application/json"} }
    fetch(url,additionalParams)
    .then(data=>{selectLobby(__lobbyId)})
    .then((a)=>{})
    .catch(error=>(console.log(error)));
}

function selectAddress() {
    const cityInput = document.getElementById("cityInput")
    const streetInput = document.getElementById("streetInput")
    const postalInput = document.getElementById("postalInput")
    const houseInput = document.getElementById("houseInput")

    const address = {
        id: 0,
        city: cityInput.value,
        street: streetInput.value,
        houseNumber: parseInt(houseInput.value),
        postalCode: postalInput.value
    };

    console.log(address);
    const url = new URL(server_url+"/order/address")
    const params = {lobbyId: __lobbyId, ownerUsername: __username};
    addParams(url, params)
    const additionalParams = {body: JSON.stringify(address), method:"POST", headers: {"content-type": "application/json"} }
    fetch(url,additionalParams)
    .then(data=>{selectLobby(__lobbyId)})
    .then((a)=>{})
    .catch(error=>(console.log(error)));
}

function addOrderItem(menuItem) {
    const orderItem = {
        id: 0,
        quantity: 1,
        menuItem: menuItem
    };
    console.log(orderItem);

    const url = new URL(server_url+"/orderitem")
    const params = {lobbyId: __lobbyId, clientUsername: __username};
    addParams(url, params)
    const additionalParams = {body: JSON.stringify(orderItem), method:"POST", headers: {"content-type": "application/json"} }
    fetch(url,additionalParams)
    .then(data=>{selectLobby(__lobbyId)})
    .then((a)=>{})
    .catch(error=>(console.log(error)));
}

function placeOrder() {
    
}

function clearContent() {
    while(left.firstChild) {
        left.removeChild(left.firstChild);
    }
    while(right.firstChild) {
        right.removeChild(right.firstChild);
    }
}

function addParams(url, params) {
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
}



fetchLobbys();