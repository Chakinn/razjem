from swagger_server.database.db import dbConnection

from swagger_server.models.restaurant import Restaurant
from swagger_server.models.address import Address
from swagger_server.models.menu import Menu

from swagger_server.database.addressDAO import AddressDAO
from swagger_server.database.menuDAO import MenuDAO

class RestaurantDAO:
    def __init__(self):
        pass

    @staticmethod
    def insert_restaurant(restaurant):
        cursor = dbConnection.cursor()

        #Insert address
        address_id = AddressDAO.insert_address(restaurant.address)
        #Insert menu
        menu_id = MenuDAO.insert_menu(restaurant.menu)

        insert_restaurant = "INSERT INTO restaurants(name,address_id,menu_id) VALUES (%(name)s,%(address_id)s,%(menu_id)s)"
        insert_restaurant_values = {"name": restaurant.name, "address_id": address_id, "menu_id": menu_id}
        cursor.execute(insert_restaurant,insert_restaurant_values)
        dbConnection.commit()
        restaurant_id = cursor.lastrowid

        cursor.close()
        return restaurant_id

    @staticmethod
    def select_restaurant(restaurant_id):
        cursor = dbConnection.cursor()

        select_restaurant_query = "SELECT name,address_id,menu_id FROM restaurants WHERE id = %(restaurant_id)s"
        cursor.execute(select_restaurant_query,{"restaurant_id": restaurant_id})
        name, address_id, menu_id = cursor.fetchone()

        address = AddressDAO.select_address(address_id)
        menu = MenuDAO.select_menu(menu_id)

        restaurant = Restaurant(restaurant_id,name,address,menu)
        cursor.close()
        return restaurant

    @staticmethod
    def select_restaurant_list():
        cursor = dbConnection.cursor()

        select_restaurant_list_query = "SELECT id,name,address_id,menu_id FROM restaurants"
        cursor.execute(select_restaurant_list_query)
        fetched_restaurant_list = cursor.fetchall()
        restaurant_list = [Restaurant(fr[0],fr[1],AddressDAO.select_address(fr[2]),MenuDAO.select_menu(fr[3])) for fr in fetched_restaurant_list]

        cursor.close()
        return restaurant_list

    @staticmethod
    def select_mock_id():
        cursor = dbConnection.cursor()

        select_mock_id_query = "SELECT id FROM restaurants WHERE name = \"xmockx\""
        cursor.execute(select_mock_id_query)
        # if found return
        fetched = cursor.fetchone()
        if fetched:
            mock_id, = fetched
            cursor.close()
            return mock_id
        #else insert mock
        else:
            mock_restaurant = Restaurant(0,"xmockx",Address(0,"x","x",0,"x"),Menu(0,[]))
            return RestaurantDAO.insert_restaurant(mock_restaurant)


