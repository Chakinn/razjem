
from swagger_server.database.db import dbConnection

from swagger_server.models.lobby import Lobby
from swagger_server.models.order import Order

from swagger_server.database.orderDAO import OrderDAO
from swagger_server.database.userDAO import UserDAO
from swagger_server.database.clientDAO import ClientDAO

class LobbyDAO:
     
    def __init__(self):
        pass

    @staticmethod 
    def insert_lobby(lobby):
        pass

    @staticmethod
    def insert_empty_lobby(owner_username):
        cursor = dbConnection.cursor()

        #Insert empty order
        order_id= OrderDAO.insert_empty_order()

        #Fetch owner id
        owner_id = UserDAO.select_id(owner_username)
        
        #Insert lobby
        insert_lobby_query = "INSERT INTO lobbys(order_id, owner_id) VALUES (%(order_id)s, %(owner_id)s)"
        cursor.execute(insert_lobby_query,{"order_id": order_id, "owner_id": owner_id})
        dbConnection.commit()
        lobby_id = cursor.lastrowid


        cursor.close()
        return lobby_id

    @staticmethod
    def updateLobby(lobby):
        cursor = dbConnection.cursor()

        #Search for addres of given order
        select_address = "SELECT address_id FROM orders WHERE id = %(order_id)s"
        cursor.execute(select_address,{"order_id": lobby.order.id})
        fetched_address = cursor.fetchall()
        #Check if address is set
        if not fetched_address or fetched_address[0][0] is None:
            #If not insert new address into db
            insert_address = "INSERT INTO addresses(city, street, house_number, postal_code) VALUES (%(city)s,%(street)s,%(house_number)s,%(postal_code)s)"
            address_values = {"city": lobby.order.address.city, "street": lobby.order.address.street, "house_number": lobby.order.address.house_number, "postal_code": lobby.order.address.postal_code}
            cursor.execute(insert_address,address_values)
            dbConnection.commit()
            address_id = cursor.lastrowid
            lobby.order.address.id = address_id
        else:
            fetched_address, = fetched_address[0]

        #Update values
        update_order = "UPDATE orders SET address_id = %(address_id)s, restaurant_id = %(restaurant_id)s, order_time = %(order_time)s, order_status = %(order_status)s where id= %(order_id)s"
        update_order_values = {"address_id": lobby.order.address.id, "restaurant_id":lobby.order.restaurant.id,
          "order_time":lobby.order.date_time, "order_status":lobby.order.status,  "order_id":lobby.order.id}

        cursor.execute(update_order,update_order_values)
        dbConnection.commit()

    @staticmethod
    def select_lobby(lobby_id):
        cursor = dbConnection.cursor()

        select_lobby = "SELECT owner_id,order_id FROM lobbys WHERE id = %(lobby_id)s"
        cursor.execute(select_lobby, {"lobby_id": lobby_id})
        owner_id,order_id = cursor.fetchone()

        owner = UserDAO.select_username(owner_id)
        order = OrderDAO.select_order(order_id)
        clients = ClientDAO.select_clients(lobby_id)

        lobby = Lobby(lobby_id,owner,order,clients)
        
        cursor.close()
        return lobby

    @staticmethod
    def select_lobby_list():
        cursor = dbConnection.cursor()

        #select_lobby_list_query = '''SELECT l.id,u.username,r.name FROM lobbys l JOIN users u ON l.owner_id=u.id 
        #                             JOIN orders o ON l.order_id = o.id JOIN restaurants r on o.restaurant_id = r.id'''

        select_lobby_ids_query = "SELECT id FROM lobbys"
        cursor.execute(select_lobby_ids_query)
        fetched_lobby_ids = cursor.fetchall()

        lobbys = []

        for fli in fetched_lobby_ids:
            lobby_id, = fli
            lobbys.append(LobbyDAO.select_lobby(lobby_id))


        cursor.close()
        return lobbys

    @staticmethod
    def select_order_id(lobby_id):
        cursor = dbConnection.cursor()

        select_order_id_query = "SELECT o.id FROM orders o JOIN lobbys l ON o.id = l.order_id WHERE l.id = %(lobby_id)s"
        cursor.execute(select_order_id_query,{"lobby_id": lobby_id})
        order_id, = cursor.fetchone()

        cursor.close()
        return order_id

    @staticmethod
    def insert_client(lobby_id, client_name):
        cursor = dbConnection.cursor()

        client_id = UserDAO.select_id(client_name)

        insert_client_query = "INSERT INTO lobbys_users VALUES(%(lobby_id)s, %(client_id)s)"
        cursor.execute(insert_client_query, {"lobby_id": lobby_id, "client_id": client_id})
        dbConnection.commit()

        cursor.close()
