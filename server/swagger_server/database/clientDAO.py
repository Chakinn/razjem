from swagger_server.database.db import dbConnection

class ClientDAO:
    def __init__(self):
        pass

    @staticmethod
    def select_clients(lobby_id):
        cursor = dbConnection.cursor()

        select_clients_query = "SELECT username FROM users u JOIN lobbys_users lu on u.id = lu.user_id JOIN lobbys l ON lu.lobby_id = l.id WHERE l.id = %(lobby_id)s"
        cursor.execute(select_clients_query, {"lobby_id": lobby_id})
        fetched_clients = cursor.fetchall()
        clients = [fc[0] for fc in fetched_clients]
        return clients


