from swagger_server.database.db import dbConnection

from swagger_server.models.order import Order
from swagger_server.models.order_item import OrderItem
from swagger_server.models.address import Address


from swagger_server.database.menuDAO import MenuDAO
from swagger_server.database.addressDAO import AddressDAO
from swagger_server.database.userDAO import UserDAO
from swagger_server.database.restaurantDAO import RestaurantDAO

class OrderDAO:
    def __init__(self):
        pass

    @staticmethod
    def insert_order(order):
        pass

    @staticmethod
    def insert_empty_order():
        cursor = dbConnection.cursor()
        
        #insert empty address
        address_id = AddressDAO.insert_address(Address(None,"x","x",0,"x"))

        mock_restaurant_id = RestaurantDAO.select_mock_id()

        #insert order
        insert_order_query = "INSERT INTO orders(address_id,restaurant_id,order_time,order_status) VALUES (%(address_id)s,%(restaurant_id)s,%(order_time)s,0)"
        cursor.execute(insert_order_query, {"address_id": address_id, "restaurant_id": mock_restaurant_id, "order_time":"x"})
        dbConnection.commit()
        order_id = cursor.lastrowid

        cursor.close()
        return order_id

    @staticmethod
    def select_order(order_id):
        cursor = dbConnection.cursor()
        #select order
        select_order_query = "SELECT address_id,order_time,restaurant_id,order_status FROM orders WHERE id = %(order_id)s"
        cursor.execute(select_order_query,{"order_id": order_id})
        address_id,order_time,restaurant_id,order_status = cursor.fetchone()

        address = None
        restaurant = None
        if address_id:
            address = AddressDAO.select_address(address_id)
        if restaurant_id:
            restaurant = RestaurantDAO.select_restaurant(restaurant_id)

        #fetch order item ids
        select_order_item_ids_query = "SELECT oi.id FROM order_items oi JOIN orders_order_items ooi ON oi.id = ooi.order_item_id JOIN orders o ON ooi.order_id = o.id WHERE o.id = %(order_id)s"
        cursor.execute(select_order_item_ids_query,{"order_id": order_id})
        fetched_order_item_ids = cursor.fetchall()

        #fetch order items
        order_item_list = []
        for order_item_id in fetched_order_item_ids:
            order_item = OrderDAO.select_order_item(order_item_id[0])
            order_item_list.append(order_item)

        order = Order(order_id,order_item_list,address,order_time,restaurant,order_status)

        cursor.close()
        return order

    @staticmethod
    def insert_order_item(order_item, order_id, client_username):
        cursor = dbConnection.cursor()

        #check order status
        status = OrderDAO.select_status(order_id)
        if status != 0:
            cursor.close()
            return         

        #Find user_id to put in db
        client_id = UserDAO.select_id(client_username)

        #Insert order_item into db
        insert_order_item = "INSERT INTO order_items(menu_item_id,user_id,quantity) VALUES(%(menu_item_id)s,%(user_id)s,%(quantity)s)"
        insert_order_item_values = {"menu_item_id":order_item.menu_item.id,"user_id":client_id,"quantity":order_item.quantity}
        cursor.execute(insert_order_item,insert_order_item_values)
        dbConnection.commit()
        order_item_id = cursor.lastrowid

        #Connect order with order_item
        insert_order_pair = "INSERT INTO orders_order_items VALUES (%(order_id)s,%(order_item_id)s)"
        insert_order_pair_values = {"order_id": order_id, "order_item_id": order_item_id}
        cursor.execute(insert_order_pair,insert_order_pair_values)

        cursor.close()
        return order_item_id

    @staticmethod
    def select_order_item(order_item_id):
        cursor = dbConnection.cursor()

        select_order_item_query = "SELECT menu_item_id,quantity,user_id FROM order_items WHERE id = %(order_id)s"
        cursor.execute(select_order_item_query,{"order_id": order_item_id})
        menu_item_id, quantity, user_id = cursor.fetchone()

        menu_item = MenuDAO.select_menu_item(menu_item_id)
        username = UserDAO.select_username(user_id)

        order_item = OrderItem(order_item_id, menu_item, quantity, username)

        cursor.close()
        return order_item

    @staticmethod
    def update_address(order_id, address):
        cursor = dbConnection.cursor()  

        #check order status
        status = OrderDAO.select_status(order_id)
        if status != 0:
            cursor.close()
            return      

        select_address_id_query = "SELECT a.id FROM addresses a JOIN orders o ON a.id = o.address_id WHERE o.id = %(order_id)s"
        cursor.execute(select_address_id_query, {"order_id": order_id})
        address.id, = cursor.fetchone()
        cursor.close()

        AddressDAO.update_address(address)


    @staticmethod
    def update_restaurant(order_id, restaurant_id):
        cursor = dbConnection.cursor()

        #check order status
        status = OrderDAO.select_status(order_id)
        if status != 0:
            cursor.close()
            return
        
        update_restaurant_query = "UPDATE orders SET restaurant_id = %(restaurant_id)s WHERE id = %(order_id)s"
        cursor.execute(update_restaurant_query, {"restaurant_id": restaurant_id, "order_id":order_id})
        dbConnection.commit()

        cursor.close()

    @staticmethod
    def update_status(order_id, status):
        cursor = dbConnection.cursor()

        update_status_query = "UPDATE orders SET order_status = %(status)s WHERE id = %(order_id)s"
        cursor.execute(update_status_query, {"status": status, "order_id": order_id})
        dbConnection.commit()

        cursor.close()

    @staticmethod
    def select_status(order_id):
        cursor = dbConnection.cursor()

        update_status_query = "SELECT order_status FROM orders WHERE id = %(order_id)s"
        cursor.execute(update_status_query, {"order_id": order_id})
        status, = cursor.fetchone()

        cursor.close()
        return status

   
    
