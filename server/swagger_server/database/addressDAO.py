from swagger_server.database.db import dbConnection
from swagger_server.models.address import Address

class AddressDAO:
    def __init__(self):
        pass

    @staticmethod
    def insert_address(address):
        cursor = dbConnection.cursor()
        insert_address = "INSERT INTO addresses(city, street, house_number, postal_code) VALUES (%(city)s,%(street)s,%(house_number)s,%(postal_code)s)"
        address_values = {"city": address.city, "street": address.street, "house_number": address.house_number, "postal_code": address.postal_code}
        cursor.execute(insert_address,address_values)
        dbConnection.commit()
        address_id = cursor.lastrowid
        return address_id

    @staticmethod
    def select_address(address_id):
        cursor = dbConnection.cursor()
        select_address_query = "SELECT * FROM addresses WHERE id = %(address_id)s"
        cursor.execute(select_address_query,{"address_id": address_id})
        fetched_address = cursor.fetchone()
        address = Address(*fetched_address)
        
        cursor.close()
        return address

    @staticmethod
    def update_address(address):
        cursor = dbConnection.cursor()
        update_address_query = "UPDATE addresses SET city = %(city)s, street = %(street)s, house_number = %(house_number)s, postal_code = %(postal_code)s WHERE id = %(address_id)s"
        cursor.execute(update_address_query, {"city": address.city, "street":address.street, "house_number":address.house_number, "postal_code": address.postal_code ,"address_id": address.id})
        dbConnection.commit()

        cursor.close()