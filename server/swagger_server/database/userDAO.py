from swagger_server.database.db import dbConnection

class UserDAO:
    def __init__(self):
        pass

    @staticmethod
    def insert_user(user):
        cursor = dbConnection.cursor()

        insert_user_query = "INSERT INTO users(username,email,password,wallet) VALUES(%(username)s,%(email)s,%(password)s,%(wallet)s)"
        cursor.execute(insert_user_query,{"username":user.username, "email":user.email, "password":user.password, "wallet":0})
        dbConnection.commit()
        user_id = cursor.lastrowid

        cursor.close()
        return user_id

    @staticmethod
    def select_username(user_id):
        cursor = dbConnection.cursor()
                
        select_username_query = "SELECT username FROM users WHERE id = %(user_id)s"
        cursor.execute(select_username_query, {"user_id": user_id})
        username, = cursor.fetchone()
        
        cursor.close()
        return username

    @staticmethod
    def select_id(username):
        cursor = dbConnection.cursor()
                
        select_username_query = "SELECT id FROM users WHERE username = %(username)s"
        cursor.execute(select_username_query, {"username": username})
        user_id, = cursor.fetchone()
        
        cursor.close()
        return user_id

    @staticmethod
    def login(username, password):
        cursor = dbConnection.cursor()

        login_query = "SELECT username,password FROM users WHERE username = %(username)s"
        cursor.execute(login_query, {"username": username})
        uname,passw = cursor.fetchone()


        if uname == username and passw == password:
            return "logged"
        else:
            return "login error"

    @staticmethod
    def select_wallet(username):
        cursor = dbConnection.cursor()

        select_wallet_query = "SELECT wallet FROM users WHERE username=%(username)s"
        cursor.execute(select_wallet_query, {"username": username})
        wallet, =cursor.fetchone()
    
        cursor.close()
        return wallet

