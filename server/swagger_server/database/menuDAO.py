from swagger_server.database.db import dbConnection

from swagger_server.models.menu import Menu
from swagger_server.models.menu_item import MenuItem


class MenuDAO:
    def __init__(self):
        pass

    @staticmethod
    def insert_menu(menu):
        cursor = dbConnection.cursor()
        insert_menu = "INSERT INTO menus VALUES ()"
        cursor.execute(insert_menu)
        dbConnection.commit()
        menu_id = cursor.lastrowid

        #Insert menu items
        insert_menu_items = "INSERT INTO menu_items(name,price,description) VALUES (%(name)s,%(price)s,%(description)s)"
        menu_item_ids = []
        for menu_item in menu.menu_items:
            menu_items_values={"name": menu_item.name, "price": menu_item.price, "description": menu_item.description}
            cursor.execute(insert_menu_items,menu_items_values)
            dbConnection.commit()
            menu_item_ids.append(cursor.lastrowid)

        #Insert menu -> menu_item pairings into helper table
        insert_menu_pairs = "INSERT INTO menus_menu_items VALUES (%(menu_id)s,%(menu_item_id)s)"
        insert_menu_values = [{"menu_id": menu_id, "menu_item_id": x} for x in menu_item_ids]
        cursor.executemany(insert_menu_pairs,insert_menu_values)
        dbConnection.commit()

        cursor.close()
        return menu_id


    @staticmethod
    def select_menu(menu_id):
        cursor = dbConnection.cursor()

        select_menu_items_query = "SELECT mi.id,name,price,description FROM menu_items mi JOIN menus_menu_items mmi ON mi.id = mmi.menu_item_id JOIN menus m ON mmi.menu_id = m.id WHERE m.id = %(menu_id)s"
        cursor.execute(select_menu_items_query, {"menu_id": menu_id})
        fetched_menu_items = cursor.fetchall()
        menu = Menu(menu_id, [MenuItem(*mi) for mi in fetched_menu_items])

        cursor.close()
        return menu

    @staticmethod
    def select_menu_item(menu_item_id):
        cursor = dbConnection.cursor()

        select_menu_item_query = "SELECT id,name,price,description FROM menu_items WHERE id = %(menu_id)s"
        cursor.execute(select_menu_item_query, {"menu_id": menu_item_id})
        fetched_menu_item = cursor.fetchone()
        menu_item = MenuItem(*fetched_menu_item)

        cursor.close()
        return menu_item
