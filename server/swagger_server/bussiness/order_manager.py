from swagger_server.models.order import Order
from swagger_server.models.order_item import OrderItem

from swagger_server.database.orderDAO import OrderDAO
from swagger_server.database.userDAO import UserDAO

class OrderManager:

    def verify_order(self, order):
    """Adds order to database
    :rtype: bool
    """
    clients = {}
    for order_item in order:
        if order_item.client_username not in clients:
            wallet = UserDAO.select_wallet(order_item.username)
            clients[order_item.client_username] = {"wallet": wallet, "items":[order_item]}
        else:
            clients[order_item.client_username]["items"].append(order_item)

    for client in clients:
        amountToPay = sum([order_item.price * order_item.quan for order_item in client["items"])
        if amountToPay > client["wallet"]:
            return False

    return True

    def place_order(self, order):
        OrderDAO.update_status(order.id, 1)