# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.order_item import OrderItem  # noqa: E501
from swagger_server.test import BaseTestCase


class TestOrderitemController(BaseTestCase):
    """OrderitemController integration test stubs"""

    def test_add_order_item(self):
        """Test case for add_order_item

        Add orderItem to given client in given lobby
        """
        body = OrderItem()
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/orderitem/{lobbyId}/{clientId}'.format(lobby_id=789, client_id=789),
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
