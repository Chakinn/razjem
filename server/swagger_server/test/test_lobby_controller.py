# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.lobby import Lobby  # noqa: E501
from swagger_server.test import BaseTestCase


class TestLobbyController(BaseTestCase):
    """LobbyController integration test stubs"""

    def test_add_lobby(self):
        """Test case for add_lobby

        Adds lobby to database
        """
        body = Lobby()
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/lobby',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_add_user_to_lobby(self):
        """Test case for add_user_to_lobby

        Add userId to lobby of given lobbyId
        """
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/lobby/{lobbyId}/{userId}'.format(lobby_id=789, user_id=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_lobby_by_id(self):
        """Test case for get_lobby_by_id

        return lobby
        """
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/lobby/{lobbyId}'.format(lobby_id=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_lobby_list(self):
        """Test case for get_lobby_list

        Returns list of all lobbys
        """
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/lobby',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
