# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.restaurant import Restaurant  # noqa: E501
from swagger_server.test import BaseTestCase


class TestRestaurantController(BaseTestCase):
    """RestaurantController integration test stubs"""

    def test_add_restaurant(self):
        """Test case for add_restaurant

        
        """
        body = Restaurant()
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/restaurant',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_restaurant_by_id(self):
        """Test case for get_restaurant_by_id

        
        """
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/restaurant/{restaurantId}'.format(restaurant_id=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_restaurant_list(self):
        """Test case for get_restaurant_list

        Returns list of all restaurants
        """
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/restaurant',
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
