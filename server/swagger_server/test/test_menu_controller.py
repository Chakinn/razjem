# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.menu import Menu  # noqa: E501
from swagger_server.test import BaseTestCase


class TestMenuController(BaseTestCase):
    """MenuController integration test stubs"""

    def test_add_menu_by_restaurant_id(self):
        """Test case for add_menu_by_restaurant_id

        
        """
        body = Menu()
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/menu/{restaurantId}'.format(restaurant_id=789),
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_menu_by_restaurant_id(self):
        """Test case for get_menu_by_restaurant_id

        
        """
        response = self.client.open(
            '/chakinos/razjemAPI/1.0.0/menu/{restaurantId}'.format(restaurant_id=789),
            method='GET')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
