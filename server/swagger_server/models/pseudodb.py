from swagger_server.models.base_model_ import Model
from swagger_server.models.address import Address
from swagger_server.models.lobby import Lobby
from swagger_server.models.menu_item import MenuItem
from swagger_server.models.menu import Menu
from swagger_server.models.order_item import OrderItem
from swagger_server.models.order import Order
from swagger_server.models.restaurant import Restaurant
from swagger_server.models.user import User

address = Address(1,"Wroclaw","Wittiga","32","12-345")
menuItem = MenuItem(1,"burger",750,"opis:super smaczny burger")
menu = Menu(1,[menuItem,menuItem,menuItem])
orderItem = OrderItem(1,menuItem,3,"clientusernameishere")
order = Order(1,[orderItem,orderItem,orderItem])
restaurant = Restaurant(1,"pizzahut",address,menu)
user = User(1,"maciej","maciek@gmail.com","123",1)
lobby = Lobby(1,"IamOwner",order,["client1","client2","client3"])

db = {
    "address": address,
    "menuItem": menuItem,
    "menu": menu,
    "order": order,
    "orderItem": orderItem,
    "restaurant": restaurant,
    "user" : user,
    "restaurants":[restaurant,restaurant,restaurant],
    "lobbys": [lobby, lobby, lobby],
}
