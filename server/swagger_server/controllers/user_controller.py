import connexion
import six

from swagger_server.models.user import User  # noqa: E501

from swagger_server.database.userDAO import UserDAO

from swagger_server import util



def create_user(body):  # noqa: E501
    """Create user

    This can only be done by the logged in user. # noqa: E501

    :param body: Created user object
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        user = User.from_dict(connexion.request.get_json())  # noqa: E501
        user_id = UserDAO.insert_user(user)
        user.id = user_id
        return body
    return "invalid registration"


def login_user(username, password):  # noqa: E501
    """Logs user into the system

     # noqa: E501

    :param username: The user name for login
    :type username: str
    :param password: The password for login in clear text
    :type password: str

    :rtype: User
    """
    return UserDAO.login(username,password)
    


def logout_user():  # noqa: E501
    """Logs out current logged in user session

     # noqa: E501


    :rtype: None
    """
    return 'do some magic!'
