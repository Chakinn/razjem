import connexion
import six

from swagger_server.models.menu import Menu  # noqa: E501
from swagger_server import util

from swagger_server.database.db import dbConnection

def add_menu_by_restaurant_id(restaurant_id, body=None):  # noqa: E501
    """add_menu_by_restaurant_id

     # noqa: E501

    :param restaurant_id: ID of restaurant which menu is returned
    :type restaurant_id: int
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        body = Menu.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def get_menu_by_restaurant_id(restaurant_id):  # noqa: E501
    """get_menu_by_restaurant_id

     # noqa: E501

    :param restaurant_id: ID of restaurant which menu is returned
    :type restaurant_id: int

    :rtype: Menu
    """
    return 'do some magic!'
