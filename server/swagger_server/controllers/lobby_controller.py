import connexion
import six

from swagger_server.database.db import dbConnection

from swagger_server.models.lobby import Lobby  # noqa: E501
from swagger_server.database.lobbyDAO import LobbyDAO 

from swagger_server import util




def add_lobby(owner_username):  # noqa: E501
    """Adds lobby to database

     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: None
    """

    lobby_id = LobbyDAO.insert_empty_lobby(owner_username)
    return join_lobby(lobby_id, owner_username)


def join_lobby(lobby_id, client_username):  # noqa: E501
    """Add username to lobby of given lobbyId

     # noqa: E501

    :param lobby_id: ID of lobby to join
    :type lobby_id: int
    :param client_username: ID of user
    :type client_username: int

    :rtype: None
    """
    LobbyDAO.insert_client(lobby_id, client_username)
    return LobbyDAO.select_lobby(lobby_id)


def get_lobby_by_id(lobby_id):  # noqa: E501
    """return lobby

     # noqa: E501

    :param lobby_id: ID of lobby which is returned
    :type lobby_id: int

    :rtype: Lobby
    """
    return LobbyDAO.select_lobby(lobby_id)


def get_lobby_list():  # noqa: E501
    """Returns list of all lobbys

     # noqa: E501


    :rtype: List[Lobby]
    """
    return LobbyDAO.select_lobby_list()

    


    
