import connexion
import six

from swagger_server.models.order import Order  # noqa: E501
from swagger_server.models.order_item import OrderItem
from swagger_server.models.address import Address
from swagger_server.models.restaurant import Restaurant

from swagger_server.database.orderDAO import OrderDAO
from swagger_server.database.lobbyDAO import LobbyDAO

from swagger_server import util

def set_restaurant(owner_username, lobby_id, body=None):
    if connexion.request.is_json:
        restaurant = Restaurant.from_dict(connexion.request.get_json())

    if restaurant:
        order_id = LobbyDAO.select_order_id(lobby_id)
        OrderDAO.update_restaurant(order_id, restaurant.id)

    return "restaurant set"

def set_address(owner_username, lobby_id, body=None):
    address = None
    if connexion.request.is_json:
        address = Address.from_dict(connexion.request.get_json())
    
    if address:
        order_id = LobbyDAO.select_order_id(lobby_id)
        OrderDAO.update_address(order_id,address)
    
    return "address set"

def add_order_item(client_username, lobby_id, body=None):
    order_item = None
    if connexion.request.is_json:
        order_item = OrderItem.from_dict(connexion.request.get_json())
        
    if order_item:
        order_id = LobbyDAO.select_order_id(lobby_id)
        OrderDAO.insert_order_item(order_item, order_id, client_username)

    return "item added"




        
            
        
