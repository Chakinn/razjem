import connexion
import six

from swagger_server.models.restaurant import Restaurant  # noqa: E501
from swagger_server.models.address import Address
from swagger_server.models.menu import Menu  
from swagger_server.models.menu_item import MenuItem  

from swagger_server.database.restaurantDAO import RestaurantDAO
from swagger_server import util

from swagger_server.models.pseudodb import db
from swagger_server.database.db import dbConnection



def add_restaurant(body=None):  # noqa: E501
    """add_restaurant

     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: None
    """

    
    if connexion.request.is_json:
        restaurant = Restaurant.from_dict(connexion.request.get_json())  # noqa: E501
        print(restaurant)
    else:
        return "wrong JSON"
    
    return RestaurantDAO.insert_restaurant(restaurant)



def get_restaurant_by_id(restaurant_id):  # noqa: E501
    """get_restaurant_by_id

     # noqa: E501

    :param restaurant_id: ID of returned restaurat
    :type restaurant_id: int

    :rtype: Restaurant
    """
    return RestaurantDAO.select_restaurant(restaurant_id)


def get_restaurant_list():  # noqa: E501
    """Returns list of all restaurants

     # noqa: E501


    :rtype: List[Restaurant]
    """

    return RestaurantDAO.select_restaurant_list()
