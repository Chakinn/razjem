import connexion
import six

from swagger_server.database.db import dbConnection

from swagger_server.models.order_item import OrderItem  # noqa: E501

from swagger_server.database.orderDAO import OrderDAO
from swagger_server.database.lobbyDAO import LobbyDAO

from swagger_server import util


def add_order_item(lobby_id, client_username, body=None):  # noqa: E501
    """Add orderItem to given client in given lobby

     # noqa: E501

    :param lobby_id: ID of lobby to join
    :type lobby_id: int
    :param client_username: username of user posting the order (for callbacks)
    :type client_username: int
    :param body: 
    :type body: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        order_item = OrderItem.from_dict(connexion.request.get_json())  # noqa: E501
    else:
       return "wrong JSON file"

    order_id = LobbyDAO.select_order_id(lobby_id)

    return OrderDAO.insert_order_item(order_item,order_id,client_username)
