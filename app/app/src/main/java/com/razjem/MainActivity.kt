package com.razjem

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.razjem.data.api.Lobby
import com.razjem.ui.lobby.add.AddLobbyActivity
import com.razjem.ui.lobby.inside.InsideLobbyActivity
import com.razjem.ui.main.MainFragment
import com.razjem.ui.main.MainViewModel
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity(), MainFragment.OnListFragmentInteractionListener {

    private lateinit var user: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.currentFragment, MainFragment.newInstance(1))
                .commitNow()
        }
        val currIntent = intent
        user = currIntent.getStringExtra("user")
        Log.d("User", user)
        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, AddLobbyActivity::class.java)
            intent.putExtra("user", user)
            startActivity(intent)
        }
    }

    //add options menu
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_1 -> {
                true
            }
            R.id.action_2 -> {
                true
            }
            R.id.action_3 -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onListFragmentInteraction(item: Lobby?) {
        val viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.joinLobby(item!!.id, user)
        val intent = Intent(this@MainActivity, InsideLobbyActivity::class.java)
        intent.putExtra("user", user)
        intent.putExtra("id", item.id)
        startActivity(intent)
        finish()
    }
}
