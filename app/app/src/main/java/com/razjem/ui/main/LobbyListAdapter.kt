package com.razjem.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.razjem.R
import com.razjem.data.api.Lobby
import com.razjem.data.api.LobbyLight
import kotlinx.android.synthetic.main.lobby_list_item.view.*

class LobbyListAdapter(
    private var items: List<Lobby>,
    private val mListener: MainFragment.OnListFragmentInteractionListener
) : RecyclerView.Adapter<LobbyListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.lobby_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: Lobby = items[position]
        holder.restaurantName.text = item.order.restaurant.name
        holder.lobbyOwner.text = item.ownerUsername
        holder.lobbyFinalizeTime.text = item.order.dateTime
        holder.joinLobbyButton.setOnClickListener {
            mListener.onListFragmentInteraction(item)
        }
    }

    fun setItems(words: List<Lobby>) {
        this.items = words
        notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView){
        val restaurantName: TextView = mView.restaurantName
        val lobbyOwner: TextView = mView.lobbyOwner
        val orderTimeLabel: TextView = mView.orderTimeLabel
        val lobbyFinalizeTime: TextView = mView.lobbyFinalizeTime
        val joinLobbyButton: Button = mView.joinLobbyButton
    }
}