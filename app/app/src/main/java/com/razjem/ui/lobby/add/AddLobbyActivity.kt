package com.razjem.ui.lobby.add

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.razjem.MainActivity
import com.razjem.R
import com.razjem.data.api.Address
import com.razjem.data.api.Lobby
import com.razjem.data.api.Order
import com.razjem.data.api.Restaurant
import com.razjem.ui.lobby.inside.InsideLobbyActivity
import kotlinx.android.synthetic.main.activity_create_lobby.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddLobbyActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewModel: AddLobbyViewModel
    private lateinit var restaurants: MutableList<Restaurant>
    private var chosenRestaurant: Restaurant? = null
    private lateinit var user: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_lobby)
        viewModel = ViewModelProviders.of(this).get(AddLobbyViewModel::class.java)
        viewManager = LinearLayoutManager(this)
        val fetchJob = viewModel.getRestaurants()
        GlobalScope.launch {
            fetchJob.join()
            runOnUiThread {
                restaurants = viewModel.getList().value!!.toMutableList()

                viewAdapter = RestaurantListAdapter(restaurants) { restaurant: Restaurant ->
                    onListFragmentInteractionListener(restaurant)
                }
                recyclerView = findViewById<RecyclerView>(R.id.restaurantList).apply {
                    layoutManager = viewManager
                    adapter = viewAdapter
                }
            }
        }
        //restaurants =
           // if (viewModel.getList().value != null) (viewModel.getList().value!!.toMutableList()) else mutableListOf()

        setLobby.setOnClickListener {
            createLobby(setLobby)
        }
        user = intent.getStringExtra("user")
    }

    fun createLobby(view: View) {
        //val date: String = datePicker.text.toString()
        val id: Long = 0
        val city: String = myCity.text.toString()
        val street: String = myStreet.text.toString()
        val building: Int = myBuilding.text.toString().toInt()
        val postal: String = postalCode.text.toString()
        val address = Address(0, city, street, building, postal)
        //val order = Order(id, emptyList(), address, date, chosenRestaurant!!, 0)
        val owner = user
        //val lobbyData = Lobby(listOf(owner), id, order, owner)
        val addJob = viewModel.addLobby(owner)
        GlobalScope.launch {
            addJob.join()
            runOnUiThread {
                val lobbyId = viewModel.getLobby().value!!.id
                GlobalScope.launch {
                    val rest = chosenRestaurant
                    val setRestaurantJob = viewModel.setRestaurant(owner,lobbyId,chosenRestaurant!!)
                    Thread.sleep(500)
                    val setAddressJob = viewModel.setAddress(owner,lobbyId,address)
                    setRestaurantJob.join()
                    setAddressJob.join()

                    val intent = Intent(this@AddLobbyActivity, InsideLobbyActivity::class.java)
                    intent.putExtra("user", user)
                    intent.putExtra("id", lobbyId)
                    runOnUiThread {
                        startActivity(intent)
                    }
                }
            }
        }

    }

    fun onListFragmentInteractionListener(item: Restaurant?) {
        chosenRestaurant = item
    }


}
