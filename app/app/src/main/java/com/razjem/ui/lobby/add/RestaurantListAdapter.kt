package com.razjem.ui.lobby.add

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.razjem.R
import com.razjem.data.api.Restaurant
import kotlinx.android.synthetic.main.restaurant_list_item.view.*


class RestaurantListAdapter(
    private var items: List<Restaurant>,
    private val mListener: (Restaurant) -> Unit
) : RecyclerView.Adapter<RestaurantListAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnLongClickListener

    init {
        mOnClickListener = View.OnLongClickListener { v ->
            val item: Restaurant = v.tag as Restaurant
            mListener(item)
            v.setBackgroundResource(android.R.drawable.dialog_holo_dark_frame)
            true
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.restaurant_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: Restaurant = items[position]
        holder.restaurant.text = item.name
        holder.city.text = item.address.city
        holder.street.text = item.address.street
        holder.building.text = item.address.houseNumber.toString()
        holder.mView.setOnClickListener {
            mListener(item)
            holder.mView.setBackgroundResource(android.R.drawable.dialog_holo_dark_frame)
        }
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val restaurant: TextView = mView.restaurantName
        val city: TextView = mView.cityName
        val street: TextView = mView.streetName
        val building: TextView = mView.buildingNumber
    }
}
