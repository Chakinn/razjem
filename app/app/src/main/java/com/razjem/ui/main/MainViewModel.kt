package com.razjem.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.razjem.data.api.*
import com.razjem.data.repos.LobbyRepo
import kotlinx.coroutines.*
import okhttp3.internal.wait
import kotlin.coroutines.CoroutineContext

class MainViewModel : ViewModel() {
    //create a new Job
    private val parentJob = Job()
    //create a coroutine context with the job and the dispatcher
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    //create a coroutine scope with the coroutine context
    private val scope = CoroutineScope(coroutineContext)

    private val repository: LobbyRepo = LobbyRepo(ApiService.razjemApi)
    private var lobbies : MutableLiveData<List<Lobby>> = MutableLiveData(emptyList())

    fun getLobbies():Job {
        return  scope.launch {
            val currentLobbies = repository.getLobbies()
            lobbies.postValue(currentLobbies)
        }


    }

    fun getList() : MutableLiveData<List<Lobby>> {
        return lobbies
    }

    fun cancelRequests() = coroutineContext.cancel()

    fun joinLobby(id : Long, user : String){
        scope.launch {
            repository.joinLobby(id,user)
        }
    }
}
