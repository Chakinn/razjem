package com.razjem.ui.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.razjem.R

class SignInActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val email = findViewById<EditText>(R.id.email)
        val signIn = findViewById<Button>(R.id.sign)
        val loading = findViewById<ProgressBar>(R.id.loading)

        signIn.isEnabled = true

        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)


        signIn.setOnClickListener {
            loading.visibility = View.VISIBLE
            loginViewModel.signIn(username.text.toString(), password.text.toString(), email.text.toString())
            finish()
        }
    }
}
