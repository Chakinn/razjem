package com.razjem.ui.lobby.inside

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.razjem.R
import com.razjem.data.api.OrderItem
import kotlinx.android.synthetic.main.order_list_item.view.*

class OrderListAdapter(
    private var items: List<OrderItem>
) : RecyclerView.Adapter<OrderListAdapter.ViewHolder>()  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.order_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.name.text = item.menuItem.name
        val str = item.menuItem.price.toString()
        val str1 = str.substring(0,str.length-2)
        val str2 = str.substring(str.length-2)
        holder.price.text = "$str1,$str2 zł"
        holder.username.text = item.clientUsername
        holder.quantity.text = item.quantity.toString()
    }

    inner class ViewHolder(val mView : View) : RecyclerView.ViewHolder(mView){
        val name : TextView = mView.itemName
        val price: TextView = mView.itemPrice
        val username : TextView = mView.username
        val quantity : TextView = mView.itemQuantity
    }
}