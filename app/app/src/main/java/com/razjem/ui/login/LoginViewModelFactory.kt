package com.razjem.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.razjem.data.LoginDataSource
import com.razjem.data.LoginRepository
import com.razjem.data.api.ApiService
import com.razjem.data.repos.UserRepo

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                loginRepository = LoginRepository(
                    dataSource = LoginDataSource(userRepo = UserRepo(ApiService.razjemApi))
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
