package com.razjem.ui.lobby.inside

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.razjem.R
import com.razjem.data.api.*
import com.razjem.ui.lobby.add.AddLobbyViewModel
import com.razjem.ui.login.LoginViewModel
import kotlinx.android.synthetic.main.activity_inside_lobby.*
import kotlinx.android.synthetic.main.menu_list_item.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

class InsideLobbyActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapterMenu: RecyclerView.Adapter<*>
    private lateinit var viewAdapterOrders: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var viewModel: InsideLobbyViewModel
    private lateinit var menuList : MutableList<MenuItem>
    private lateinit var orderList : MutableList<OrderItem>
    private lateinit var cachedLobby : Lobby
    private lateinit var user : String
    private var lobbyId : Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inside_lobby)
        viewModel = ViewModelProviders.of(this).get(InsideLobbyViewModel::class.java)
        val intent = intent
        lobbyId = intent.getLongExtra("id", 0)
        user = intent.getStringExtra("user")
        val job = viewModel.setLobby(lobbyId)

        GlobalScope.launch {
            job.join()
            runOnUiThread {
                cachedLobby = viewModel.getLobby().value!!
                menuList = cachedLobby.order.restaurant.menu.menuItems.toMutableList()
                orderList = cachedLobby.order.orderItems.toMutableList()
                viewAdapterMenu = MenuListAdapter(menuList) { item : MenuItem, quantity: Int -> addItemToOrder(item, quantity)}
                viewAdapterOrders = OrderListAdapter(orderList)
                switchOrder()
            }

        }
        viewManager = LinearLayoutManager(this)

        menuViewButton.setOnClickListener {
            switchMenu()
        }
        orderView.setOnClickListener {
            switchOrder()
        }
        GlobalScope.launch {
            while(isActive) {
                delay(10000)
                reloadLobby()
            }
        }
    }

    private fun addItemToOrder(item : MenuItem, quantity: Int){
        val toAdd = OrderItem(lobbyId, item, quantity, user)
        viewModel.addItem(user, toAdd)
    }

    private fun reloadLobby() {
        val refreshJob = viewModel.lobbyRefresh()
        GlobalScope.launch {
            refreshJob.join()
            orderList.clear()
            orderList.addAll(viewModel.getLobby().value!!.order.orderItems)

            runOnUiThread {
                viewAdapterOrders.notifyDataSetChanged()
                viewAdapterMenu.notifyDataSetChanged()
            }
        }


    }



    private fun switchMenu(){
        recyclerView = currentList.apply{
            layoutManager = viewManager
            adapter = viewAdapterMenu
        }
    }

    private fun switchOrder(){
        recyclerView = currentList.apply{
            layoutManager = viewManager
            adapter = viewAdapterOrders
        }
    }
}
