package com.razjem.ui.lobby.add

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.razjem.data.api.Address
import com.razjem.data.api.ApiService
import com.razjem.data.api.Lobby
import com.razjem.data.api.Restaurant
import com.razjem.data.repos.LobbyRepo
import com.razjem.data.repos.OrderRepo
import com.razjem.data.repos.RestaurantRepo
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class AddLobbyViewModel : ViewModel() {

    private val restaurantRepo : RestaurantRepo = RestaurantRepo(ApiService.razjemApi)
    private val lobbyRepo : LobbyRepo = LobbyRepo(ApiService.razjemApi)
    private val orderRepo : OrderRepo = OrderRepo(ApiService.razjemApi)
    private var restaurants : MutableLiveData<List<Restaurant>> = MutableLiveData(emptyList())
    private var myLobby : MutableLiveData<Lobby> = MutableLiveData()

    //create a new Job
    private val parentJob = Job()
    //create a coroutine context with the job and the dispatcher
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    //create a coroutine scope with the coroutine context
    private val scope = CoroutineScope(coroutineContext)

    fun getRestaurants():Job{
        return scope.launch {
            val newRestaurants: List<Restaurant>? = restaurantRepo.getRestaurants()
            restaurants.postValue(newRestaurants)
        }
    }

    fun addLobby(ownerUsername: String): Job{
        return scope.launch {
            val updatedLobby = lobbyRepo.addLobby(ownerUsername)
            myLobby.postValue(updatedLobby)
        }
    }

    fun setRestaurant(username: String, lobbyId:Long, restaurant: Restaurant): Job {
        return scope.launch {
            orderRepo.setRestaurant(username,lobbyId, restaurant)
        }
    }

    fun setAddress(username: String, lobbyId:Long, address: Address): Job {
        return scope.launch {
            orderRepo.setAddress(username,lobbyId, address)
        }
    }

    fun cancelRequests() = coroutineContext.cancel()

    fun getList() : MutableLiveData<List<Restaurant>>{
        return restaurants
    }

    fun getLobby() : MutableLiveData<Lobby>{
        return myLobby
    }
}