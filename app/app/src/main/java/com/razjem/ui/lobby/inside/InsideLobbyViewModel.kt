package com.razjem.ui.lobby.inside

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.razjem.data.api.*
import com.razjem.data.repos.LobbyRepo
import com.razjem.data.repos.OrderRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class InsideLobbyViewModel : ViewModel(){

    private val orderRepo : OrderRepo = OrderRepo(ApiService.razjemApi)
    private var menu : MutableLiveData<List<MenuItem>> = MutableLiveData(emptyList())
    private var orderItems : MutableLiveData<List<OrderItem>> = MutableLiveData(emptyList())
    private val lobbyRepo : LobbyRepo = LobbyRepo(ApiService.razjemApi)
    private var myLobby : MutableLiveData<Lobby> = MutableLiveData()
    //create a new Job
    private val parentJob = Job()
    //create a coroutine context with the job and the dispatcher
    private val coroutineContext : CoroutineContext get() = parentJob + Dispatchers.Default
    //create a coroutine scope with the coroutine context
    private val scope = CoroutineScope(coroutineContext)

    fun setMenu(new : List<MenuItem>){
        menu.postValue(new)
    }

    fun setOrderItems(new : List<OrderItem>){
        orderItems.postValue(new)
    }

    fun setLobby(id : Long):Job{
        return scope.launch {
            val updatedLobby = lobbyRepo.getLobby(id)
            myLobby.postValue(updatedLobby)
        }
    }

    fun getMenu() : MutableLiveData<List<MenuItem>>{
        return menu
    }

    fun getOrderItems() : MutableLiveData<List<OrderItem>>{
        return orderItems
    }

    fun getLobby() : MutableLiveData<Lobby>{
        myLobby.value
        return myLobby
    }

    fun lobbyRefresh(): Job{
        return scope.launch {
            val updatedLobby = lobbyRepo.getLobby(myLobby.value!!.id)
            myLobby.postValue(updatedLobby)
            //setOrderItems(myLobby.value!!.order.orderItems)
        }
    }

    fun addItem(username: String, new : OrderItem){
        scope.launch {
            orderRepo.addItemToOrder(myLobby.value!!.id, username, new)
            lobbyRefresh()
        }
    }

}