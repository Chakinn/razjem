package com.razjem.ui.lobby.inside

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.razjem.R
import com.razjem.data.api.MenuItem
import kotlinx.android.synthetic.main.menu_list_item.view.*

class MenuListAdapter(
    private var items: List<MenuItem>,
    private val mListener: (MenuItem,Int) -> Unit
) : RecyclerView.Adapter<MenuListAdapter.ViewHolder>()  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.menu_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.name.text = item.name
        val str = item.price.toString()
        val str1 = str.substring(0,str.length-2)
        val str2 = str.substring(str.length-2)
        holder.price.text = "$str1,$str2 zł"
        holder.description.text = item.description
        holder.add.setOnClickListener {
            val quantity = holder.quantity.text.toString().toInt()
            mListener(item,quantity)
        }
    }

    inner class ViewHolder(val mView : View) : RecyclerView.ViewHolder(mView){
        val name : TextView = mView.itemName
        val price: TextView = mView.itemPrice
        val description : TextView = mView.itemDescription
        val quantity : EditText = mView.itemQuantity
        val add : Button = mView.addToOrder
    }
}