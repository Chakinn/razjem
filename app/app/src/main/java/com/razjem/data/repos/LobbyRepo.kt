package com.razjem.data.repos

import com.razjem.data.api.ApiInterface
import com.razjem.data.api.Lobby
import com.razjem.data.api.LobbyLight

class LobbyRepo(private val apiInterface: ApiInterface) : ApiRepository() {
    suspend fun getLobbies() : List<Lobby>? {
        return safeApiCall(
            call = {apiInterface.getLobbies().await()},
            error = "Couldn't get lobbies"
        )
    }

    suspend fun addLobby(ownerUsername: String) : Lobby? {
        return safeApiCall(
            call = {apiInterface.addLobby(ownerUsername).await()},
            error = "Couldn't add lobby"
        )
    }

    suspend fun getLobby(id : Long) : Lobby? {
        return safeApiCall(
            call = {apiInterface.getLobby(id).await()},
            error = "Couldn't get lobby"
        )
    }

    suspend fun joinLobby(id : Long, clientUsername : String) : Lobby?{
        return safeApiCall(
            call = {apiInterface.joinLobby(id,clientUsername).await()},
            error = "Couldn't join lobby"
        )
    }
}