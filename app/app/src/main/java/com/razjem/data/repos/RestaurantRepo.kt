package com.razjem.data.repos

import com.razjem.data.api.ApiInterface
import com.razjem.data.api.Restaurant

class RestaurantRepo(private val apiInterface: ApiInterface) : ApiRepository() {
    suspend fun getRestaurants() : List<Restaurant>? {
        return safeApiCall(
            call = {apiInterface.getRestaurants().await()},
            error = "Couldn't get restaurants"
        )
    }
}