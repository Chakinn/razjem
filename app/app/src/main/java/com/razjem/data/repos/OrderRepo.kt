package com.razjem.data.repos

import com.razjem.data.api.*

class OrderRepo(private val apiInterface: ApiInterface) : ApiRepository(){
    suspend fun addItemToOrder(lobbyId : Long, clientUsername : String, new: OrderItem) : String? {
        return safeApiCall(
            call = {apiInterface.addItemToOrder(lobbyId, clientUsername, new).await()},
            error = "Couldn't add item to order"
        )
    }

    suspend fun setRestaurant(ownerUsername: String, lobbyId: Long, restaurant: Restaurant) : String? {
        return safeApiCall(
            call = {apiInterface.setRestaurant(ownerUsername, lobbyId, restaurant).await()},
            error = "Couldn't add order"
        )
    }

    suspend fun setAddress(ownerUsername: String, lobbyId: Long, address: Address) : String? {
        return safeApiCall(
            call = {apiInterface.setAddress(ownerUsername, lobbyId, address).await()},
            error = "Couldn't add order"
        )
    }
}