package com.razjem.data.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class User(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "username") val username: String,
    @field:Json(name = "email") val email: String,
    @field:Json(name = "password") val password: String,
    @field:Json(name = "wallet") val wallet: Int
)
@JsonClass(generateAdapter = true)
data class Address(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "city") val city: String,
    @field:Json(name = "street") val street: String,
    @field:Json(name = "houseNumber") val houseNumber: Int,
    @field:Json(name = "postalCode") val postalCode: String
)
@JsonClass(generateAdapter = true)
data class MenuItem(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "price") val price: Int,
    @field:Json(name = "description") val description: String
)
@JsonClass(generateAdapter = true)
data class Menu(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "menuItems") val menuItems: List<MenuItem>
)
@JsonClass(generateAdapter = true)
data class Restaurant(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "address") val address: Address,
    @field:Json(name = "menu") val menu: Menu
)
@JsonClass(generateAdapter = true)
data class OrderItem(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "menuItem") val menuItem: MenuItem,
    @field:Json(name = "quantity") val quantity: Int,
    @field:Json(name = "clientUsername") val clientUsername: String
)
@JsonClass(generateAdapter = true)
data class Order(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "orderItems") val orderItems: List<OrderItem>,
    @field:Json(name = "address") val address: Address,
    @field:Json(name = "dateTime") val dateTime: String,
    @field:Json(name = "restaurant") val restaurant: Restaurant,
    @field:Json(name = "status") val status: Int
)
@JsonClass(generateAdapter = true)
data class Lobby(
    @field:Json(name = "clients") val clients: List<String>,
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "order") val order: Order,
    @field:Json(name = "ownerUsername") val ownerUsername: String
)
@JsonClass(generateAdapter = true)
data class LobbyLight(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "ownerUsername") val ownerUsername: String,
    @field:Json(name = "restaurantName") val restaurantName : String,
    @field:Json(name = "dateTime") val dateTime: String
)
