package com.razjem.data.repos

import android.util.Log
import com.razjem.data.api.ApiInterface
import com.razjem.data.api.User

class UserRepo(private val apiInterface: ApiInterface) : ApiRepository() {
    suspend fun logIn(username: String, password: String): String? {
        return safeApiCall(
            call = { apiInterface.logInUser(username, password).await() },
            error = "Invalid username/password supplied"
        )
    }

    suspend fun signIn(new : User) : User? {
        return safeApiCall(
            call = {apiInterface.addUser(new).await()},
            error = "Couldn't sign in"
        )
    }

    suspend fun logout() : Void? {
        return safeApiCall(
            call = {apiInterface.logout().await()},
            error = "Couldn't sign in"
        )
    }
}