package com.razjem.data.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @POST("user")
    fun addUser(@Body new : User) : Deferred<Response<User>>

    @GET("user/login")
    fun logInUser(@Query("username") username : String, @Query("password") password: String) : Deferred<Response<String>>

    @GET("user/logout")
    fun logout() : Deferred<Response<Void>>

    @POST("restaurant")
    fun addRestaurant(@Body new : Restaurant) : Deferred<Response<Restaurant>>

    @GET("restaurant")
    fun getRestaurants() : Deferred<Response<List<Restaurant>>>

    @GET("restaurant/{restaurantId}")
    fun getRestaurant(@Path("restaurantId") restaurantId : Long) : Deferred<Response<Restaurant>>

    @GET("menu/{restaurantId}")
    fun getMenuFromRestaurant(@Path("restaurantId") restaurantId : Long) : Deferred<Response<Menu>>

    @POST("menu/{restaurantId}")
    fun addMenuToRestaurant(@Path("restaurantId") restaurantId : Long) : Deferred<Response<Menu>>

    @POST("orderitem")
    fun addItemToOrder(@Query("lobbyId") lobbyId : Long, @Query("clientUsername") clientUsername : String, @Body new: OrderItem) : Deferred<Response<String>>

    @POST("order/restaurant")
    fun setRestaurant(@Query("ownerUsername") ownerUsername: String, @Query("lobbyId") lobbyId: Long, @Body restaurant: Restaurant) : Deferred<Response<String>>

    @POST("order/address")
    fun setAddress(@Query("ownerUsername") ownerUsername: String, @Query("lobbyId") lobbyId: Long, @Body address: Address) : Deferred<Response<String>>

    @GET("lobby")
    fun getLobbies() : Deferred<Response<List<Lobby>>>

    @GET("lobby/new")
    fun addLobby(@Query("ownerUsername") ownerUsername: String) : Deferred<Response<Lobby>>

    @GET("lobby/join")
    fun joinLobby(@Query("lobbyId") lobbyId : Long, @Query("clientUsername") clientUsername : String) : Deferred<Response<Lobby>>

    @GET("lobby/{lobbyId}")
    fun getLobby(@Path("lobbyId") lobbyId : Long) : Deferred<Response<Lobby>>

}