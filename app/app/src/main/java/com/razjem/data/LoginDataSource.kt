package com.razjem.data

import android.util.Log
import com.razjem.data.api.User
import com.razjem.data.model.LoggedInUser
import com.razjem.data.repos.UserRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.IOException
import kotlin.coroutines.CoroutineContext

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource(private val userRepo: UserRepo) {

    suspend fun login(username: String, password: String): Result<LoggedInUser> {
        try {
            val auth = userRepo.logIn(username, password)
            Log.d("XD" , "Czy zalogowano? "+ auth)
            if(!auth.equals("logged")) {
                throw Throwable("Couldn't log in.")
            }
            val loggedUser = LoggedInUser(isLogged = auth!!)
            return Result.Success(loggedUser)
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    suspend fun logout() {
        userRepo.logout()
    }

    suspend fun signIn(new : User) : Result<String>{
        try {
            val auth = userRepo.signIn(new)
            if(auth != null) {
                throw Throwable("Couldn't sign in.")
            }
            return Result.Success("Created user")
        } catch (e: Throwable) {
            return Result.Error(IOException("Error ", e))
        }
    }
}
